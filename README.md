# LawDisTrA

LawDisTrA is an agent-based system that was conceived to distribute lawsuits in the Brazilian Superior Labor Court among judges considering the auditability aspect.

## Multi-Agent GUI

![LawDisTra GUI](imgs/agentes-gui.png)

LawDisTrA graphical user interface was implemented using Java Swing. For each agent, a GUI was created so as to monitor its execution. The interface for the PMA agent was developed to allow the manager user to monitor and control the other agents (PAs, MAs, and DAs).

## LawDisTrA Auditor Web Module

![LawDisTrA Auditor Web Module](imgs/auditor-distribuicao-gui-2.png)

The “Distribution Auditor” was implemented as a web interface that allows the access to lawsuits and distribution data to provide transparency to users. 

[LawDisTrA-Auditor](LawDisTrA-Auditor/)

## Multi-Agent Architecture

![LawDisTra Architecture](imgs/Architecture.png)

