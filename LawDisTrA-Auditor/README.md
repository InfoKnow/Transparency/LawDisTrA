## LawDisTrA Auditor Web Module

The “Distribution Auditor” was implemented as a web interface that allows the access to lawsuits and distribution data to provide transparency to users. 


![LawDisTrA Auditor Web Module](imgs/auditor-distribuicao-gui-1.png)


![LawDisTrA Auditor Web Module](imgs/auditor-distribuicao-gui-2.png)


![LawDisTrA Auditor Web Module](imgs/auditor-distribuicao-gui-3.png)
